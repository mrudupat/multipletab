import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SHA256 } from 'crypto-js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  utterancesByLanguage: { [key: string]: string[] } = {
    english: [] // Initialize the 'english' array
  };
  backendApiUrl: string = 'http://localhost:3000'; // Replace with your backend API URL

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    // Fetch utterances from the backend API upon initialization
    this.fetchUtterances();
  }

  fetchUtterances(): void {
    const storedUtterances = localStorage.getItem('utterances');
    if (storedUtterances) {
      this.utterancesByLanguage = JSON.parse(storedUtterances);
    } else {
      this.http.get<any[]>(this.backendApiUrl + '/api/utterances').subscribe(
        response => {
          // Clear the existing utterances
          this.utterancesByLanguage = {
            english: []
          };

          // Populate the utterances from the response
          response.forEach(item => {
            const { language, utterance } = item;
            if (!this.utterancesByLanguage[language]) {
              this.utterancesByLanguage[language] = [];
            }
            this.utterancesByLanguage[language].push(utterance);
          });

          // Store the utterances in localStorage
          localStorage.setItem('utterances', JSON.stringify(this.utterancesByLanguage));
        },
        error => {
          console.error(error);
        }
      );
    }
  }

  handleUtteranceSubmitted(data: { language: string, utterance: string }): void {
    if (!this.utterancesByLanguage[data.language]) {
      this.utterancesByLanguage[data.language] = [];
    }

    // Generate hash code
    const hashCode = SHA256(data.utterance).toString();

    // Combine hash code and utterance with a space separator
    const combinedText = data.utterance.trim(); // Remove the hashcode from the utterance


    // Write the combined text to file
    const filePath = `GenerateAudiosByTextForMultiLanguage1/ena_text/${hashCode}.txt`;
    this.writeFile(filePath, combinedText);

    // Add the utterance to the language array
    this.utterancesByLanguage[data.language].push(combinedText); // <-- Change 'data.utterance' to 'combinedText'

    // Prepare the data to be sent through the API
    const requestData = {
      language: data.language,
      utterance: combinedText, // <-- Change 'data.utterance' to 'combinedText'
      hashCode: hashCode,
    };

    // Make an HTTP request to trigger Python execution
    this.http.post(this.backendApiUrl + '/script', requestData).subscribe(
      response => {
        // Handle the response from the backend
        console.log(response);
      },
      error => {
        // Handle error, if any
        console.error(error);
      }
    );
    //localStorage.setItem('utterances', JSON.stringify(this.utterancesByLanguage));
  }


  writeFile(filePath: string, content: string): void {
    this.http.post(this.backendApiUrl + '/writeFile', { filePath, content}).subscribe(
      response => {
        console.log('File write success:', response);
      },
      error => {
        console.error('File write error:', error);
      }
    );
  }
}
